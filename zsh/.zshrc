# Lines configured by zsh-newuser-install
HISTFILE=$XDG_CACHE_HOME/zsh/zsh_history
HISTSIZE=1000
SAVEHIST=1000
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/bruno/.zshrc'

# Checking if ls colors is usable and aliasing it
ls --color=auto &>/dev/null && alias ls='ls --color=auto'

# Checking if exa is installed and aliasing it
which exa &>/dev/null && alias ls='exa --color=auto --icons'

alias et="emacs -nw"

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Command line prompt:

setopt PROMPT_SUBST
PROMPT="%F{red}[%f%F{yellow}%n%f%F{green}@%f%F{blue}%m%f %F{magenta}%~%f%F{red}]%f$ "
