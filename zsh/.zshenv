# Setting the EDITOR envvar
export EDITOR="nvim"

# Setting the TERMINAL envvar
export TERMINAL="kitty"

# Setting XDG envvar
export XDG_DATA_HOME="$HOME/.local/share"

export XDG_CONFIG_HOME="$HOME/.config"

export XDG_STATE_HOME="$HOME/.local/state"

export XDG_CACHE_HOME="$HOME/.cache"

# Enable dark mode for GTK

export GTK_THEME=Adwaita:dark

# Making ~/.cache/zsh
ls $HOME/.cache/zsh &>/dev/null || mkdir -p $HOME/.cache/zsh

# setting ~/.local/bin on the path
ls $HOME/.local/bin &>/dev/null || mkdir -p $HOME/.local/bin

if [ $? -eq 0 ] 
then
	export PATH="$HOME/.local/bin:$PATH"
else
	echo 'Failed to create ~/.local/bin'
fi

# Setting CARGO_HOME envvar
export CARGO_HOME="$XDG_DATA_HOME/cargo"

# Setting the RUSTUP_HOME envvar
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"

# Setting the ZDOTDIR envvar
ZDOTDIR="$XDG_CONFIG_HOME/zsh"

# Disabling the less history
export LESSHISTFILE="-"

# Setting the INPUTRC envvar
export INPUTRC="$XDG_CONFIG_HOME/inputrc"

# Setting the GNUPGHOME envvar
export GNUPGHOME="$XDG_DATA_HOME/gnupg"

# Loading cargo tools
export PATH="$PATH:$XDG_DATA_HOME/cargo/bin"

# Loading cargo/env if it exists
if [ ls '$XDG_DATA_HOME/cargo/env' &>/dev/null $0 -eq 0 ]
then
	. "/home/bruno/.local/share/cargo/env"
fi

# Setting the WINEPREFIX envvar
export WINEPREFIX="$XDG_DATA_HOME/wine"
