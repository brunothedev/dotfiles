vim.g.mapleader = ' ' -- Space

local tl_builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', tl_builtin.find_files, {})

vim.keymap.set('n', '<leader>b', ':vnew<CR>')
vim.keymap.set('n', '<leader>hb', ':new<CR>')

