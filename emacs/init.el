(require 'package)
(add-to-list
 'package-archives
 '("melpa" . "https://melpa.org/packages/")
 t) ;; Adding MELPA

(setq
 package-selected-packages
 '(lsp-mode
   gruvbox-theme
   evil-mode
etc))

(add-to-list 'load-path "~/.config/emacs/elisp/")

;; To load a lsp server, use:
;; (load "lsp/<lsp_server>")

(setq make-backup-files nil) ;; Prevent the creation of backup files

(load-theme 'gruvbox t)

(custom-set-faces
 '(default ((t (:height 120 :family "Fira Code"))))
)

;; Enabling evil-mode
(require 'evil)
(evil-mode 1)
