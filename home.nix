{ config, pkgs, ... }:
{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "brunothedev";
  home.homeDirectory = "/home/brunothedev";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.11"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # pkgs.hello
    pkgs.loc   
    pkgs.prismlauncher
    pkgs.lm_sensors
    pkgs.yt-dlp
    pkgs.anki-bin
    pkgs.gimp
    pkgs.qemu
    pkgs.qbittorrent
    pkgs.freecad
    pkgs.kicad

    pkgs.distrobox

    # Hacking!
    pkgs.pmbootstrap
    pkgs.heimdall
    pkgs.android-tools

    # I'm whitehat pls fbi i am just a child pls
    pkgs.nmap

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    (pkgs.writeShellScriptBin "update-dotfiles" ''
      cp -r ${config.home.homeDirectory}/.dotfiles/* /etc/nixos/ && nixos-rebuild switch
    '')
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    ".zshenv".source = ./zsh/.zshenv;
  };

  xdg.configFile = {
    "zsh/.zshrc".source = ./zsh/.zshrc;
    "waybar".source = ./waybar;
    "kitty".source = ./kitty;
    "nvim".source = ./nvim;
    "hypr".source = ./hypr;
    "tofi".source = ./tofi;
    "zathura".source = ./zathura;
  };

  # Home Manager can also manage your environment variables through
  # 'home.sessionVariables'. If you don't want to manage your shell through Home
  # Manager then you have to manually source 'hm-session-vars.sh' located at
  # either
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  ~/.local/state/nix/profiles/profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/brunothedev/etc/profile.d/hm-session-vars.sh
  #
  home.sessionVariables = {
    ANKI_WAYLAND = "1";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
